package com.euedge.coolstuff;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.euedge.coolstuff.CoolStuffShopApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CoolStuffShopApplication.class)
public class CoolStuffShopApplicationTests {

    // TODO: create unit tests for main functionality

	@Test
	public void contextLoads() {
	}

}
